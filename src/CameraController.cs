using System;
using CityTest.common;
using CityTest.interaction.worldcursor;
using Godot;
using Godot.Collections;
using Object = Godot.Object;

namespace CityTest {
    public class CameraController : Camera {
        private bool _isTurning = false;
        private static readonly Logger Logger = new Logger("CameraController");
        private static readonly Vector3 PitchVector = new Vector3(1, 0, 0);
        private const double Sensitivity = 0.3;
        private double _yaw = 0;
        private double _pitch = 0;

        private AbstractWorldCursor _cursor = null;

        public void SetCursor(AbstractWorldCursor cursor) {
            try { GetParent().RemoveChild(_cursor); }
            catch (Exception e) { Logger.DBUG("Not removing null cursor"); }
            _cursor = cursor;
            GetParent().AddChild(_cursor);
        }
        public void ClearCursor() {
            GetParent().RemoveChild(_cursor);
            _cursor = null;
        }

        public override void _Input(InputEvent ev) {
            switch (ev) {
                case InputEventMouseButton evButton: {
                    switch (evButton.ButtonIndex) {
                        case (int) ButtonList.Middle when ev.IsPressed(): {
                            Logger.DBUG("M3 down, camera rotation on");
                            _isTurning = true;
                            Input.SetMouseMode(Input.MouseMode.Captured);
                            break;
                        }
                        case (int) ButtonList.Middle: {
                            Logger.DBUG("M3 up, camera rotation off");
                            _isTurning = false;
                            Input.SetMouseMode(Input.MouseMode.Visible);
                            break;
                        }
                        case (int) ButtonList.WheelDown when ev.IsPressed(): {
                            TranslateObjectLocal(Vector3.Back);
                            break;
                        }
                        case (int) ButtonList.WheelUp when ev.IsPressed(): {
                            TranslateObjectLocal(Vector3.Forward);
                            break;
                        }
                        case (int) ButtonList.Right when _cursor != null && ev.IsPressed(): {
                            //ClearCursor();
                            _cursor.OnRClick();
                            break;
                        }
                        case (int) ButtonList.Left when _cursor == null && ev.IsPressed(): {
                            SetCursor(RoadNetworkCursor.Instance);
                            break;
                        }
                        case (int) ButtonList.Left when _cursor != null && ev.IsPressed(): {
                            _cursor.OnLClick();
                            break;
                        }
                    }

                    break;
                }
                case InputEventMouseMotion evMotion when _isTurning: {
                    _yaw = _yaw * Sensitivity + evMotion.Relative.x * Sensitivity;
                    _pitch = _pitch * Sensitivity + evMotion.Relative.y * Sensitivity;

                    RotateY(Mathf.Deg2Rad((float) -_yaw));
                    RotateObjectLocal(PitchVector, Mathf.Deg2Rad((float) -_pitch));
                    break;
                }
                case InputEventMouseMotion evMotion when _cursor != null: {
                    Vector3? ray = Raycast();
                    if (ray != null) {
                        _cursor.Translation = ray.Value;
                        _cursor.OnMove();
                    }
                    break;
                }
            }
        }

        public Vector3? Raycast(int length=3000) {
            Dictionary result = RawRaycast(length);
            
            if (result.Contains("position")) {
                return (Vector3) result["position"];
            }
            return null;
        }

        public Object ObjectRaycast(int length = 3000) {
            Dictionary result = RawRaycast(length);

            if (result.Contains("collider") && result["collider"] != null) {
                return (Object) result["collider"];
            }
            return null;
        }

        private Dictionary RawRaycast(int length) {
            Vector3 origin = ProjectRayOrigin(GetViewport().GetMousePosition());
            Vector3 target = origin + ProjectRayNormal(GetViewport().GetMousePosition()) * length;
            PhysicsDirectSpaceState state = GetWorld().DirectSpaceState;

            return state.IntersectRay(origin, target);            
        }
    }
}
