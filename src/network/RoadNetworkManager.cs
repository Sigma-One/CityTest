using Godot;
using System;
using System.Collections.Generic;
using CityTest.common;
using CityTest.network;

public class RoadNetworkManager : Spatial {
    public List<RoadPiece> Edges = new List<RoadPiece>();
    public List<RoadNode> Nodes = new List<RoadNode>();
    private static Logger _logger = new Logger("RoadNetworkManager");
    
    public override void _Ready() {
;
    }

    public void AddNode(Vector3 position) {
        _logger.DBUG("Adding node");
        RoadNode node = new RoadNode(position);
        Nodes.Add(node);
        AddChild(node);

        if (Nodes.Count > 1) {
            RoadPiece edge = (RoadPiece) Preload.Instance.RoadSegmentScene.Instance();
            AddChild(edge);
            Edges.Add(edge);
            edge._Init(Nodes[Nodes.IndexOf(node)-1], node);
        }
    }
//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
