using Godot;

namespace CityTest.network {
    public class RoadNode : Spatial {
        public int ConnCount;
        public RoadNode(Vector3 position) {
            Translation = position;
        }
    }
}