using Godot;
using System;
using CityTest.network;

public class RoadPiece : CSGPolygon {
    private RoadNode _endNode;
    private RoadNode _startNode;
    private Path _path;

    public void setPoints(RoadNode end, RoadNode start) {
        
    }

    public override void _Ready() {
        _path = new Path();
        _path.Curve = new Curve3D();
    }

    public void Destroy() {
        _endNode.ConnCount--;
        _startNode.ConnCount--;

        if (_endNode.ConnCount == 0) {
            ((RoadNetworkManager) GetNode("/root/Root/World/NetworkHolder/RoadNetworkManager")).Nodes.Remove(_endNode);
            _endNode.QueueFree();
        }
        
        if (_startNode.ConnCount == 0) {
            ((RoadNetworkManager) GetNode("/root/Root/World/NetworkHolder/RoadNetworkManager")).Nodes.Remove(_startNode);
            _startNode.QueueFree();
        }
        
        
        ((RoadNetworkManager) GetNode("/root/Root/World/NetworkHolder/RoadNetworkManager")).Edges.Remove(this);
        QueueFree();
    }
    
    public void _Init(RoadNode end, RoadNode start) {
        Mode = ModeEnum.Path;
        _path.Curve.AddPoint(start.Translation);
        _path.Curve.AddPoint(end.Translation);
        AddChild(_path);
        PathNode = _path.GetPath();

        end.ConnCount++;
        _endNode = end;
        start.ConnCount++;
        _startNode = start;
    }
}
