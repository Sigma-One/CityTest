using Godot;

namespace CityTest.common {
    public class Logger {
        private readonly string _label;

        public Logger(string label) {
            this._label = label; }

        private void Write(string message, string level) {
            GD.Print("[" + level + "][" + this._label + "] " + message);
        }

        public void DBUG(string message) { this.Write(message, "DBUG"); }
        public void INFO(string message) { this.Write(message, "INFO"); }
        public void WARN(string message) { this.Write(message, "WARN"); }
        public void FAIL(string message) { this.Write(message, "FAIL"); }
    }
}
