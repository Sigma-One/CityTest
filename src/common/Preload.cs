using Godot;

namespace CityTest.common {
    public class Preload {
        public static Preload Instance = new Preload();
        
        public PackedScene RoadSegmentScene = (PackedScene) GD.Load("res://scn/RoadPiece.tscn");
    }
}