using Godot;

namespace CityTest.interaction.worldcursor {
    public abstract class AbstractWorldCursor : Spatial {
        public abstract CSGShape CursorObject { get; }

        public abstract void OnLClick();
        public abstract void OnRClick();
        public abstract void OnMove();
    }
}