using System;
using CityTest.common;
using CityTest.network;
using Godot;
using Object = Godot.Object;

namespace CityTest.interaction.worldcursor {
    public class RoadNetworkCursor : AbstractNetworkCursor {
        public static readonly RoadNetworkCursor Instance = new RoadNetworkCursor();
        private readonly CSGSphere _cursorSphere = new CSGSphere();
        public override CSGShape CursorObject => _cursorSphere;
        

        public override void OnLClick() {
            Vector3? ray = ((CameraController) GetNode("/root/Root/Camera")).Raycast();
            if (ray != null) {
                ((RoadNetworkManager) GetNode("/root/Root/World/NetworkHolder/RoadNetworkManager"))
                    .AddNode(ray.Value);
            }
        }

        public override void OnRClick() {
            Object ray = ((CameraController) GetNode("/root/Root/Camera")).ObjectRaycast();

            if (ray is RoadPiece piece) {
                GD.Print("test foobar lmao");
                piece.Destroy();
            }
        }

        public override void OnMove() {
        }

        private RoadNetworkCursor() {
            AddChild(_cursorSphere);
        }
    }
}